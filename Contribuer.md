# Guide de contribution au site Decidim de Nantes en commun.e.s

Nous explicitons ici les étapes à suivre pour contribuer à notre site internet [Decidim](decidim.nantesencommun.org/). La présente version de ce guide se veut concise, nous produirons à terme une version plus détaillée pour expliquer ces étapes et donner une initiation au développement web pour ceux qui le souhaitent.

Les modifications les plus facilement accessibles sont les textes et la mise en page (css et javascript), nous détaillerons ce processus dans la suite.

Pour que ce guide soit accessible au plus grand nombre, nous avons simplifié au maximum le processus d'installation.
Il doit pouvoir être suivi sans prérequis en informatique, mais il vous faudra néanmoins être à l'aise avec le langage utilisé pour certaines modifications.
Voici quelques tutoriels utiles si vous débutez :

- [css]() pour modifier le style d'une page web.
- [javascript]() pour pour l'interactif.

Decidim étant une application basée sur le framework Ruby on Rails, ses pages web ne sont pas codées simplement en html, mais en Embedded Ruby (erb).
Si vous souhaitez apporter des modifications à la structure du site il sera nécessaire de maîtriser un minimum [Ruby on Rails](https://guides.rubyonrails.org/getting_started.html) (et [html]()).

## Installation

Par soucis de simplicité, nous utiliserons une machine virtuelle VirtualBox avec toutes les dépendances nécessaires préinstallées, ainsi qu'une base de données préconfigurée. 

### Mise en place de la machine virtuelle

- Télécharger [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- Télécharger [notre machine virtuelle]() avec Decidim préinstallé
- Importer la machine dans VirtualBox
- Configurer la connection réseau. Network -> Attached to : Bridged adapter -> Name voi plus tard.
- Configurer dossier partagé. A faire dans la vm ?

### Première connection à la machine

- Démarrer la machine virtuelle
- Ouvrir un terminal
- Se connecter avec ssh :

```
ssh deploy@ip
```

Entrez le mot de passe : "deploy"

Vous êtes maintenant connecté à votre machine virtuelle, c'est depuis ce terminal que vous la controllerez !
- rentrer nom/email git

### Installation de l'instance de Decidim

Une fois connecté à la machine virtuelle, entrez les commandes suivantes.

- Cloner notre instance de Decidim :

```
git clone https://gitlab.com/nantes-en-commun/decidim
```

- Copier le fichier application.yml pour utiliser la base de données :

```
cp ~/application.yml ~/decidim/config/
```

Voilà ! Notre instance locale est prête à être utilisée !

## Processus de contribution 

Les étapes suivantes sont celles que vous devrez répéter à chaque fois que vous voudrez contribuer.

### Synchronisation de l'instance

Le code du site et de ses dépendances est mis à jour continuellement par d'autres développeurs, il faut donc mettre à jour votre copie locale pour travailler correctement : c'est le but de cette section.
- Démarrer la machine virtuelle. 
- S'y connecter avec ssh (comme pour l'installation).
- Mettre à jour son système :

```
sudo apt update
```

Rentrez de nouveau le mot de passe : "deploy". Puis :

```
sudo apt upgrade
```

- Mise à jour de l'instance :

```
cd ~/decidim
git pull origin master
```

- attention : erreur si copie de travail pas clean ?

```
rails db:migrate
```


### Lancement du site

Cette section explique comment naviguer dans votre instance locale pour visualiser vos changements en temps réel.

- ip add ?
- Lancer le serveur web sur la machine :

```
cd ~/decidim
rails s -b 0.0.0.0
```

Vous pouvez maintenant vous connecter au site local dans votre navigateur web ! Entrez l'url http://ip:3000.

### Fichiers à modifier

- textes
- stylesheet css (possibilité de faire plusieurs style sheets ? Genre avec un include)
- javascript
- erb pour ceux qui maîtrisent

### Enregistrement dans git

- commit
- pull request
