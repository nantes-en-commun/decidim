# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

every 1.day, at: '3:00 am' do
  command "cd $HOME/decidim/current && RAILS_ENV=production bundle exec rake decidim:metrics:all"
end

every 1.day, at: '3:01 am' do
  command "cd $HOME/decidim/current && RAILS_ENV=production bundle exec rake decidim:delete_data_portability_files"
end

every 1.day, at: '3:02 am' do
  command "cd $HOME/decidim/current && RAILS_ENV=production bundle exec rake decidim:open_data:export"
end

every 1.day, at: '3:03 am' do
  command "cd $HOME/decidim/current && RAILS_ENV=production bundle exec rake decidim_meetings:clean_registration_forms"
end

every 5.minute do
  command  "if ! [ -s $HOME/decidim/current/tmp/pids/delayed_job.pid ]; then
              RAILS_ENV=production $HOME/decidim/current/bin/delayed_job start
            fi"
end
